<?php 
/* 
Template Name: OnePager 
*/ ?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1; user-scalable=no">

  <?php wp_head(); ?>
</head>

<body <?php body_class(); ?> >

<div class="op-sections">
  <?php the_content(); ?>
</div>

<?php wp_footer(); ?>
</body>
</html>
