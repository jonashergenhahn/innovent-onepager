<?php
// this file should be in the theme
Onepager::getOptionPanel()
        ->tab( 'general', 'Generals' )
        ->add(
          
          array(
            'name'   => 'section_title_size',
            'label'  => 'Section Title Size',
            'append' => 'px',
            'value'  => '44',
          ),
          
          array(
            'name'    => 'font_size',
            'label'   => 'Default Font Size',
            'append'  => 'px',
            'value'   => '18',
          ),

          array( 'name' => 'favicon', 'type' => 'image' ),
          array(
            'name'        => 'google_analytics',
            'type'        => 'textarea',
            'label'       => 'Google Analytics',
            'placeholder' => 'Paste your code here',
          )
        )
        ->tab( 'styles', 'Styles' )
        ->add(
          array(
            'name'    => 'color',
            'type'    => 'colorpalette',
            'presets' => 'default',
            'value'   => array(
              'primary'   => '#555555',
              'secondary' => '#999999',
              'accent'    => '#fbc600',
            ),
          ),
          array('label' => 'Title Font Settings', 'name' => 'divider_title_font', 'type' => 'divider'),
          array(
            'name' => 'title_font_style',
            'label' => 'Section Title Style',
            'placeholder' => 'Font name'
          ),
          array(
            'name' => 'google_title_font',
            'type' => 'switch',
            'label' => 'Google Font for Title'
          ),

          array('label' => 'Font Settings', 'name' => 'divider_font', 'type' => 'divider'),
          array(
            'name' => 'font_style',
            'label' => 'Section Title Style',
            'placeholder' => 'Font name'
          ),
          array(
            'name' => 'google_font',
            'type' => 'switch',
            'label' => 'Google Font for Title'
          )
        )
        ->tab( 'advanced' )
        ->add(
          array( 'name' => 'full_screen', 'label' => 'Full Screen', 'type' => 'switch'),
          array( 'name' => 'onepager_debug', 'label' => 'Development Mode', 'type' => 'switch', 'value' => false )
        );

add_action( 'wp_head', function () { ?>
  <link rel="icon" href="<?php echo Onepager::getOption( 'favicon' ) ?>">

  <?php if(Onepager::getOption('google_font')): ?>
    <!-- Font-family -->
    <link rel="stylesheet" type="text/css"
            href="https://fonts.googleapis.com/css?family=<?php echo str_replace(' ', '+', Onepager::getOption('font_style'));?>">

    <style type="text/css">
      html, body {
        font-family: '<?php echo Onepager::getOption('font_style');?>', serif;
      }
    </style>
  <?php elseif(!empty(Onepager::getOption('font_style'))): ?>
    <style type="text/css">
      html, body {
        font-family: '<?php echo Onepager::getOption('font_style');?>';
      }
    </style>
  <?php endif; ?>

  <?php if(Onepager::getOption('google_title_font')): ?>
    <!-- Font-family -->
    <link rel="stylesheet" type="text/css"
            href="https://fonts.googleapis.com/css?family=<?php echo str_replace(' ', '+', Onepager::getOption('title_font_style'));?>">

    <style type="text/css">
      h1, h2, h3, h4, h5, h6 {
        font-family: '<?php echo Onepager::getOption('title_font_style');?>', serif;
      }
    </style>
  <?php elseif(!empty(Onepager::getOption('title_font_style'))): ?>
    <style type="text/css">
      h1, h2, h3, h4, h5, h6 {
        font-family: '<?php echo Onepager::getOption('title_font_style');?>';
      }
    </style>
  <?php endif; ?>

  <!-- Default Font Size -->
  <?php if(Onepager::getOption( 'font_size')):?>
    <style type="text/css">
      html, body {
        font-size: <?php echo Onepager::getOption('font_size');?>px;
      }
    </style>
  <?php endif; ?>

  <!--  Google Analytics-->
  <?php echo Onepager::getOption( 'google_analytics' ); ?>

  <!--  Full pager-->
  <?php if ( Onepager::getOption( 'full_screen' ) ): ?>
    <script>
      jQuery(document).ready(function ($) {
        $(".op-sections").fullpage({
          sectionSelector: ".op-section",
          css3: true,
          scrollingSpeed: 700,
          scrollBar: true,
          navigation: true
        });
      });
    </script>
  <?php endif; ?>
<?php }, 100 );


if ( Onepager::getOption( 'full_screen' ) ) {
  add_action( 'wp_enqueue_scripts', function () {
    $q = onepager()->asset();
    $q->script( 'op-slimscroll', op_asset( 'assets/js/jquery.slimscroll.min.js' ) );
    $q->script( 'op-fullpage', op_asset( 'assets/js/jquery.fullPage.js' ) );

    $q->style( 'op-fullpage', op_asset( 'assets/css/jquery.fullPage.css' ) );
  } );
}
