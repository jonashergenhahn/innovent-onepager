<?php

if ( ! defined( 'ONEPAGER_URL' ) ) {
  define( 'ONEPAGER_URL', get_stylesheet_directory_uri()."/innovent-onepager" );
}

require( __DIR__ . "/innovent-onepager.php" );
